(function ($) {
    "use strict";
    var bg = $("#bg"),
        svg = $("#svg"),
        text = $("#text"),
        png = $("#png"),
        alter = $("#alter"),
        ul = $("ul"),
        li = $("li"),
        dl = $("#dl3"),
        qr = $("#qr"),
        body = $("body"),
        textPos = [
            {
                //1024 * 576 768
                left: [690, 690, 690, 690],
                bottom: [300, 300, 300, 300]
            },
            {
                //1280 * 720
                left: [210, 860, 720, 636],
                bottom: [20, 560, 440, 670]
            },
            {
                //1440 * 810
                left: [210, 990, 850, 710],
                bottom: [0, 530, 530, 650]
            },
            {
                //1600 * 900
                left: [210, 1100, 970, 770],
                bottom: [20, 550, 560, 660]
            },
            {
                //1920 * 1080
                left: [320, 1350, 1230, 900],
                bottom: [50, 730, 650, 740]
            }
        ],
        pngPos = [
            {
                //1024 * 576 768
                left: [0, 0, 0, 0],
                bottom: [0, 0, 0, 0]
            },
            {
                //1280 * 720
                left: [55, 850, 700, 525],
                bottom: [125, 210, 290, 155]
            },
            {
                //1440 * 800
                left: [170, 970, 830, 590],
                bottom: [145, 240, 325, 180]
            },
            {
                //1600 * 900
                left: [280, 1090, 950, 660],
                bottom: [160, 260, 360, 200]
            },
            {
                //1920 * 1080
                left: [490, 1320, 1200, 790],
                bottom: [200, 310, 430, 240]
            }
        ],
        svgPos = [
            {
                //1024 * 576 768
                left: [0, 0, 0, 0],
                bottom: [0, 0, 0, 0]
            },
            {
                //1280 * 720
                left: [55, 850, 700, 455],
                bottom: [75, 160, 240, 105]
            },
            {
                //1440 * 800
                left: [150, 970, 830, 520],
                bottom: [102, 190, 275, 130]
            },
            {
                //1600 * 900
                left: [280, 1090, 950, 590],
                bottom: [110, 210, 310, 150]
            },
            {
                //1920 * 1080
                left: [490, 1320, 1200, 720],
                bottom: [150, 260, 380, 160]
            }
        ],
        bgWidth = [1024, 1280, 1440, 1600, "100%"],
        p = 0, //position of text
        w = 0, //whether fit the height
        s = 0, //whether show svg
        url = [
            "http://mms.qianlong.com/",
            "http://mms.qianlong.com/mobile.html"
        ];

    function setPosition() {
        var wWidth = window.innerWidth,
            wHeight = window.innerHeight;
        if (wWidth <= 1024) {
            p = 0;
            s = 1;
        } else if (wWidth <= 1280) {
            p = 1;
            s = 0;
        } else if (wWidth <= 1440) {
            p = 2;
            s = 0;
        } else if (wWidth <= 1600) {
            p = 3;
            s = 0;
        } else {
            p = 4;
            s = 0;
        }
        if (wHeight > 1219 / 1870 * wWidth) {
            w = 1;
        } else {
            w = 0;
        }
        if (w) {
            bg.width(wHeight * 1870 / 1219);
        } else {
            bg.width(bgWidth[p]);
        }
        if (s) {
            svg.hide();
        } else {
            svg.show();
        }
        body.height(bg.height());
        svg.height(bg.height());
        text.height(bg.height());
        png.height(bg.height());
        text.children().each(function (index, element) {
            $(element).css({
                left: textPos[p].left[index],
                bottom: textPos[p].bottom[index]
            });
        });
        png.children().each(function (index, element) {
            $(element).css({
                left: pngPos[p].left[index],
                bottom: pngPos[p].bottom[index]
            });
        });
        svg.children().each(function (index, element) {
            $(element).css({
                left: svgPos[p].left[index],
                bottom: svgPos[p].bottom[index]
            });
            $(element).width(png.children().eq(index).width() + 70);
            if (index === 0) {
                $(element).height(png.children().eq(index).height() + 50 + 35);
            } else {
                $(element).height(png.children().eq(index).height() + 50);
            }
        });
    }
    window.onload = setPosition;
    window.onresize = setPosition;

    function setSection(e) {
        var ele = $(e.target),
            i = li.index(ele) - 1;
        li.removeClass("active");
        ele.addClass("active");
        svg.children().hide();
        text.children().hide();
        alter.children().hide();
        if (i >= 0) {
            if (!s) {
                svg.children().eq(i).show();
            }
            text.children().eq(i).fadeIn();
            body.animate({scrollTop: text.children().eq(i).offset().top - 20}, 800);
        } else {
            alter.children("svg").show();
            alter.children("div").fadeIn();
            body.animate({scrollTop: alter.children("div").offset().top - 20}, 800);
        }
    }

    function setSection2(e) {
        var ele = $(e.target),
            i = li.index(ele) - 1;
        li.removeClass("active");
        ele.addClass("active");
        png.children().hide();
        text.children().hide();
        alter.children().hide();
        if (i >= 0) {
            if (!s) {
                png.children().eq(i).fadeIn();
            }
            text.children().eq(i).fadeIn();
            body.animate({scrollTop: text.children().eq(i).offset().top - 20}, 800);
        } else {
            alter.children("img").fadeIn();
            alter.children("div").fadeIn();
            body.animate({scrollTop: alter.children("div").offset().top - 20}, 800);
        }
    }

    dl.on("click", function () {
        qr.toggle(400);
    });
    li.eq(0).click();

}(window.jQuery));